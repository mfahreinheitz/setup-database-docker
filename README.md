# setup-docker

## Getting started

# Docker

Download for Mac or Windows:
https://www.docker.com/products/docker-desktop

Download for Linux Ubuntu:
https://docs.docker.com/engine/install/ubuntu/

Download for Linux Debian:
https://docs.docker.com/engine/install/debian/

Download for Linux CentOS:
https://docs.docker.com/engine/install/centos/

Download for Linux Fedora:
https://docs.docker.com/engine/install/fedora/

# Create Docker Network - testNetwork (We will put all our containers in the same network)

docker network create --attachable -d overlay testNetwork

- [ ] [MySql] [volume in docker not in hardisk]

```
Run in Docker:
docker run -it -d --name mysql-container \
-p 3306:3306 --network testNetwork \
-e MYSQL_ROOT_PASSWORD=root \
--restart always \
-v mysql_data_container:/var/lib/mysql  \
mysql:latest
```

- [ ] [MongoDB] [volume in docker not in hardisk]

```
Run in Docker:
docker run -it -d --name mongo-container \
-p 27017:27017 --network testNetwork \
--restart always \
-v mongodb_data_container:/data/db \
mongo:latest 
```

Download Client Tools – Robo 3T:
https://robomongo.org/download

# building docker images for the rest of the microservices (Excecute command in directory where Dockerfile of each project is located)

```
sudo docker build -t project-service
```

# Docker file service [for jdk 14]

```
FROM openjdk:14-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
```

# building a docker image

```
sudo docker build -t project-service .
```

# list existing docker containers in running state

```
sudo docker ps
```

# obtaining networking details about a container, such as network name

```
sudo docker inspect mongo-container -f "{{json .NetworkSettings.Networks }}"
```

# run an image as a docker container

```
sudo docker run -d -p 8084:8084 --name project-service --network testNetwork -e "SPRING_PROFILES_ACTIVE=docker" --restart always --link mongo-container project-service
```

# log into docker hub repo

```
sudo docker login
```

# taging a local docker image to a docker hub repo image

```
sudo docker tag project-service imam/joel:project-service
```

# push a docker image to a docker hub repository

```
sudo docker push imam/joel:project-service
```

# list all docker containers that are running on a docker host

```
sudo docker container ls
```

# remove a docker container

```
sudo docker rm <container ID / container name>
```

# list all docker images that have been downloaded to a docker host

```
sudo docker image ls
```

# remove a docker image

```
sudo docker image rm <image name / image ID>
```

# pull / download a docker image to a docker host

```
sudo docker pull imam/joel:project-service
```

# run a docker image as a container using a docker hub repo image

```
sudo docker run -d -p 8084:8084 --name project-service --network testNetwork -e "SPRING_PROFILES_ACTIVE=docker" --restart always --link mongo-container imam/joel:project-service
```


## Setup Docker Compose [Manage Container Docker]

https://docs.docker.com/compose/install/

Docker Swarm Init
https://docs.docker.com/engine/reference/commandline/swarm_init/

# run one or more docker containers with docker compose (Execute command in directory where docker-compose.yml file is located)

```
sudo docker-compose up -d
```

# stop one or more docker containers with docker compose (Execute command in directory where docker-compose.yml file is located)

```
sudo docker-compose down
```

#Initialize docker swarm

```
sudo docker swarm init
```

# run one or more docker containers with stack deploy (Execute command in directory where docker-compose-stack.yml file is located)

```
sudo docker stack deploy --compose-file docker-compose-stack.yml service
```

# list containers that are deployed as services using stack deploy

```
sudo docker services ls
```

# remove all services that are deployed as part of stack (Where service is the name of the stack)

```
sudo docker stack rm service
```






